# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Takuya Noguchi <takninnovationresearch@gmail.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=ansible-base
_pkgname=ansible-base
# renovate: datasource=pypi depName=ansible-base
pkgver=2.10.10
pkgrel=99
pkgdesc='A configuration-management, deployment, task-execution, and multinode orchestration framework'
url='https://ansible.com/'
arch='noarch'
license='GPL-3.0-or-later'

# renovate: datasource=repology depName=alpine_3_13/python3 versioning=loose
depends="$depends python3=3.8.10-r0"
# renovate: datasource=repology depName=alpine_3_13/py3-yaml versioning=loose
depends="$depends py3-yaml=5.3.1-r2"
# renovate: datasource=repology depName=alpine_3_13/py3-paramiko versioning=loose
depends="$depends py3-paramiko=2.7.2-r0"
# renovate: datasource=repology depName=alpine_3_13/py3-jinja2 versioning=loose
depends="$depends py3-jinja2=2.11.2-r0"
# renovate: datasource=repology depName=alpine_3_13/py3-markupsafe versioning=loose
depends="$depends py3-markupsafe=1.1.1-r3"
# renovate: datasource=repology depName=alpine_3_13/py3-cryptography versioning=loose
depends="$depends py3-cryptography=3.3.2-r0"

# renovate: datasource=repology depName=alpine_3_13/python3 versioning=loose
makedepends="$makedepends python3-dev=3.8.10-r0"
# renovate: datasource=repology depName=alpine_3_13/py3-setuptools versioning=loose
makedepends="$makedepends py3-setuptools=51.3.3-r0"

source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
options='!check'

build() {
	python3 -B setup.py -q build
}

package() {
	python3 -B setup.py -q install --prefix=/usr --root="$pkgdir"
}
sha512sums="
b599b498a20b77cd3346f5b865b5a0436cdd835af250e8cc4ed20d61ac6a4929bf4ceef9a95c6e7b8e825c5f513d33ea32b24661c58e2f1d285db0569d71fc1b  ansible-base-2.10.10.tar.gz
"
