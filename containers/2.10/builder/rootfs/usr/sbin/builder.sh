#!/usr/bin/env sh

set -eux
ABUILD_USER='abuild'
HOME_DIR="/home/${ABUILD_USER}"
PACKAGE_DIR="${HOME_DIR}/packages"

build() {
  cd "${HOME_DIR}"
  abuild-keygen -a -i
  packageFiles=$(find . -name APKBUILD | sort)

  for APKBUILD in $packageFiles; do
    export APKBUILD
    abuild -r -P "${PACKAGE_DIR}"
  done
}

prepare() {
  apk add alpine-sdk sudo
  adduser --ingroup abuild -D "${ABUILD_USER}"
  echo "${ABUILD_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
  chown -R "${ABUILD_USER}:" "${HOME_DIR}"
  echo "${PACKAGE_DIR}/${ABUILD_USER}" >> /etc/apk/repositories
}

main() {
  case "$1" in
    'build')
      build
      ;;
    'prepare')
      prepare
      ;;
    *)
      exit 1
      ;;
  esac
}

main "$@"
